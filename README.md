# OLIP upgrade

## How to run it

```shell
docker run --rm \
  --name "olip-upgrade" \
  --network "olip" \
  --privileged \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v "/home/ideascube/deploy:/deploy" \
  -v /olip-files:/olip-files \
  -v /:/host \
  -v /run:/run \
  -e WE_NEED_VERSIONING=true \
  --rm \
  registry.gitlab.com/bibliosansfrontieres/olip-ng/bsf-core/olip-upgrade:latest
```

## How to run it (force upgrade for old OLIP versions)

```shell
docker run --rm \
  --name "olip-upgrade" \
  --network "olip" \
  --privileged \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v "/home/ideascube/deploy:/deploy" \
  -v /olip-files:/olip-files \
  --rm \
  registry.gitlab.com/bibliosansfrontieres/olip-ng/bsf-core/olip-upgrade:latest
```

## Force upgrade

For "old" OLIP versions, OLIP upgrade will run through `force-upgrade.sh`.

This upgrade will reset all the cube and restart from scratch (without deleting actual OLIP data).

## Common upgrade

For common OLIP versions, OLIP upgrade will run through `common-upgrade.sh`.

This upgrade will use `reset.sh` script from deploy, and reinstall OLIP stack using deploy without deleting actual OLIP data.
