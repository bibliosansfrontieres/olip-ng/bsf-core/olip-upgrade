#!/bin/bash

# If this container doesn't have /host directory, we need to restart it with this volume

if [ ! -d "/host" ]; then
    container_id=$(bash /olip-upgrade/utils/get-container-id.sh)
    olip_upgrade_image=$(docker inspect --format "{{.Config.Image}}" "$container_id")
    docker run --rm --name olip-upgrade-with-host --privileged --volumes-from olip-upgrade --volume /:/host --volume /run:/run --detach "$olip_upgrade_image"
    exit 0
fi

# OLIP using new deploy version are sending WE_NEED_VERSIONING variable
# So old OLIPs will run through force-upgrade

if [ -z "$WE_NEED_VERSIONING" ]; then
    bash /olip-upgrade/force-upgrade.sh
    exit 0
fi

bash /olip-upgrade/common-upgrade.sh
exit 0