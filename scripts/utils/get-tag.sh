#!/bin/bash

tag="latest"

# All conditions are "inverted" so we trust the last condition first

# Get it from an olip-files backup
last_deploy_backup=$(bash /olip-upgrade/utils/get-last-deploy-backup.sh)
if [ -n "$last_deploy_backup" ]; then
    if [ -f "$last_deploy_backup/olip-files/install/tag" ]; then
        tag=$(cat "$last_deploy_backup/olip-files/install/tag")
    fi
fi

# Get it from olip-files
if [ -f "/olip-files/install/tag" ]; then
    tag=$(cat /olip-files/install/tag)
fi

echo "$tag"