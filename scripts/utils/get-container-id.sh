#!/bin/bash

container_id=$(docker ps --filter "name=olip-upgrade-with-host" --format "{{.ID}}")

# if no container id
if [ -z "$container_id" ]; then
    container_id=$(docker ps --filter "name=olip-upgrade" --format "{{.ID}}")
fi

echo "$container_id"