#!/bin/bash

ssid=""

# All conditions are "inverted" so we trust the last condition first

# Get it from an olip-files backup
last_deploy_backup=$(bash /olip-upgrade/utils/get-last-deploy-backup.sh)
if [ -n "$last_deploy_backup" ]; then
    if [ -f "$last_deploy_backup/olip-files/deploy/ssid" ]; then
        ssid=$(cat "$last_deploy_backup/olip-files/deploy/ssid")
    fi
fi

# Get it from olip-files
if [ -f "/olip-files/deploy/ssid" ]; then
    ssid=$(cat /olip-files/deploy/ssid)
fi

echo "$ssid"