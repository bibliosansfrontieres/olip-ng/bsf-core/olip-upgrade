#!/bin/bash

language=""

# All conditions are "inverted" so we trust the last condition first

# Get it from old .olip-dashboard.env
if [ -f "/deploy/.olip-dashboard.env" ]; then
    language=$(grep "VITE_DEFAULT_LANGUAGE" /deploy/.olip-dashboard.env | cut -d "=" -f 2)
fi

# Get it from new .olip-dashboard.env
if [ -f "/deploy/envs/.olip-dashboard.env" ]; then
    language=$(grep "VITE_DEFAULT_LANGUAGE" /deploy/envs/.olip-dashboard.env | cut -d "=" -f 2)
fi

# Get it from an olip-files backup
last_deploy_backup=$(bash /olip-upgrade/utils/get-last-deploy-backup.sh)
if [ -n "$last_deploy_backup" ]; then
    if [ -f "$last_deploy_backup/olip-files/deploy/language" ]; then
        language=$(cat "$last_deploy_backup/olip-files/deploy/language")
    fi
fi

# Get it from olip-files
if [ -f "/olip-files/install/language" ]; then
    language=$(cat /olip-files/install/language)
fi

echo "$language"