#!/bin/bash

deploy_args=""
container_id=$(bash /olip-upgrade/utils/get-container-id.sh)
host_deploy_path=$(docker inspect "$container_id" | jq -r '.[] | .Mounts[] | select(.Destination == "/deploy") | .Source')
vm_save_id=$(bash /olip-upgrade/utils/get-vm-save-id.sh)
language=$(bash /olip-upgrade/utils/get-language.sh)
interface=$(bash /olip-upgrade/utils/get-interface.sh)
ssid=$(bash /olip-upgrade/utils/get-ssid.sh)

if [ -n "$vm_save_id" ]; then
    deploy_args="$deploy_args --vm-save-id $vm_save_id"
else
    exit 1
fi

if [ -n "$language" ]; then
    deploy_args="$deploy_args --language $language"
fi

if [ -n "$interface" ]; then
    deploy_args="$deploy_args --interface $interface"
fi

if [ -n "$ssid" ]; then
    deploy_args="$deploy_args --ssid $ssid"
fi

if /olip-upgrade/utils/is-bsf-core.sh; then
    deploy_args="$deploy_args --bsf-core"
fi

deploy_args="$deploy_args --deploy-path $host_deploy_path"
deploy_args=$(echo "$deploy_args" | sed 's/^[ \t]*//;s/[ \t]*$//')

echo "$deploy_args"