#!/bin/bash

last_deploy_backup=""

if [ -d "/olip-backups" ]; then
    last_timestamp=$(
        for file in /olip-backups/deploy-*; do
            [[ $file =~ ([0-9]+) ]] && echo "${BASH_REMATCH[1]}"
        done | sort -n | tail -1 || echo ""
    )
    backup_directory="/olip-backups/deploy-$last_timestamp"
    if [ -n "$last_timestamp" ] && [ -d "$backup_directory" ]; then
        last_deploy_backup="$backup_directory"
    fi
fi

echo "$last_deploy_backup"