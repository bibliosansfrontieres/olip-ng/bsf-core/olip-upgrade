#!/bin/bash

vm_save_id=""

# All conditions are "inverted" so we trust the last condition first

# Get it from old .olip-api.env
if [ -f "/deploy/.olip-api.env" ]; then
    vm_save_id=$(grep "VIRTUAL_MACHINE_SAVE_ID" /deploy/.olip-api.env | cut -d "=" -f 2)
fi

# Get it from new .olip-api.env
if [ -f "/deploy/envs/.olip-api.env" ]; then
    vm_save_id=$(grep "VIRTUAL_MACHINE_SAVE_ID" /deploy/envs/.olip-api.env | cut -d "=" -f 2)
fi

# Get it from new .olip-api.env
if [ -f "/olip-files/virtual-machine-save-id" ]; then
    vm_save_id=$(jq -r '.virtualMachineSaveId' "/olip-files/virtual-machine-save-id")
fi

# Get it from an olip-files backup
last_deploy_backup=$(bash /olip-upgrade/utils/get-last-deploy-backup.sh)
if [ -n "$last_deploy_backup" ]; then
    if [ -f "$last_deploy_backup/olip-files/deploy/virtual-machine-save-id" ]; then
        vm_save_id=$(cat "$last_deploy_backup/olip-files/deploy/virtual-machine-save-id")
    fi
fi

# Get it from olip-files
if [ -f "/olip-files/deploy/virtual-machine-save-id" ]; then
    vm_save_id=$(cat /olip-files/deploy/virtual-machine-save-id)
fi

echo "$vm_save_id"