#!/bin/bash

if [ -f "/olip-files/bsf-core" ]; then
    exit 0
fi

if [ -f "/olip-files/deploy/bsf-core" ]; then
    exit 0
fi

if [ -f "/olip-files/install/bsf-core" ]; then
    exit 0
fi

exit 1