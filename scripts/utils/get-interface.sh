#!/bin/bash

interface=""

# All conditions are "inverted" so we trust the last condition first

# Get it from an olip-files backup
last_deploy_backup=$(bash /olip-upgrade/utils/get-last-deploy-backup.sh)
if [ -n "$last_deploy_backup" ]; then
    if [ -f "$last_deploy_backup/olip-files/deploy/interface" ]; then
        interface=$(cat "$last_deploy_backup/olip-files/deploy/interface")
    fi
fi

# Get it from olip-files
if [ -f "/olip-files/deploy/interface" ]; then
    interface=$(cat /olip-files/deploy/interface)
fi

echo "$interface"