#!/bin/bash

BRANCH="${BRANCH:-$(bash /olip-upgrade/utils/get-branch.sh)}"
TAG="${TAG:-$(bash /olip-upgrade/utils/get-tag.sh)}"
DEPLOY_ARGS=$(bash /olip-upgrade/utils/get-deploy-args.sh)

DEPLOY_BRANCH=${DEPLOY_BRANCH:-$BRANCH}
NETPROBE_BRANCH=${NETPROBE_BRANCH:-$BRANCH}
MACSWITCHER_BRANCH=${MACSWITCHER_BRANCH:-$BRANCH}

# This variable is used by systemd
# We need it to use host systemd
export SYSTEMCTL_FORCE_BUS=1

say() {
    echo >&2 "[$(basename "$0")] $*"
}

check_internet() {
    if [ -f "/olip-files/network/internet" ]; then
        say "We have internet !"
        return 0
    fi
    say "No internet connection, exiting..."
    exit 1
}

clone_deploy_repo() {
    say "Clone deploy repository"
    git clone --branch "$DEPLOY_BRANCH" https://gitlab.com/bibliosansfrontieres/olip-ng/bsf-core/deploy.git /updated-deploy
}

pull_netprobe_repo() {
    say "Pull netprobe repository"
    cd /host/usr/local/src/netprobe || exit 1
    git pull
}

pull_macswitcher_repo() {
    say "Pull macswitcher repository"
    cd /host/usr/local/src/macswitcher || exit 1
    git pull
}

pull_new_images() {
    say "Pull new images"
    bash /updated-deploy/scripts/install.sh -t "$TAG" --skip-config
}

remove_all_containers() {
    say "Remove all containers"
    containers_to_kill=$(docker ps -a --format '{{.ID}} {{.Names}}' | grep -vE "(olip-deploy|olip-upgrade)" | awk '{print $1}')
    docker kill $containers_to_kill
    docker rm $containers_to_kill
}

update_netprobe() {
    say "Update netprobe"
    cd "/host/usr/local/src/netprobe" || exit
    PREFIX="/host" bash ./install.sh
}

update_macswitcher() {
    say "Update macswitcher"
    cd "/host/usr/local/src/macswitcher" || exit
    PREFIX="/host" bash ./install.sh
}

reload_systemd() {
    say "Reload systemd"
    systemctl daemon-reload
}

copy_old_database() {
    say "Copy old database and files"
    if [ -d "/deploy/olip" ]; then
        mkdir -p /host/olip
        rsync -a --remove-source-files --progress --info=progress2 /deploy/olip/ /host/olip/
        say "Old database and files copied successfully"
    else
        say "Source folder /deploy/olip does not exist"
    fi
}

replace_old_deploy() {
    say "Replace old deploy"
    rm -rf /deploy/*
    rm -rf /deploy/.[!.]* /deploy/..?* 2>/dev/null
    mv /updated-deploy/* /updated-deploy/.[!.]* /updated-deploy/..?* /deploy 2>/dev/null
    rm -rf /updated-deploy
}

remove_olip_files() {
    say "Remove olip-files"
    TARGET_FOLDER="/olip-files"
    EXCLUDED_FOLDERS=("network" "vpn")

    cd "$TARGET_FOLDER" || exit
    for item in *; do
        if [[ ! " ${EXCLUDED_FOLDERS[@]} " =~ " $item " ]]; then
            rm -rf "$item"
        fi
    done
}

install_olip() {
    say "Install OLIP"
    bash /deploy/scripts/install.sh --skip-pull -t "$TAG"
}

deploy_olip() {
    say "Deploy OLIP"
    # shellcheck disable=SC2086
    bash /deploy/scripts/deploy.sh $DEPLOY_ARGS --skip-download
}

docker_prune() {
    say "Prune docker"
    docker system prune -a --force
}

# Need internet connection
check_internet || exit 1
clone_deploy_repo || exit 1
pull_netprobe_repo || exit 1
pull_macswitcher_repo || exit 1
pull_new_images || exit 1

# Don't need internet connection anymore
remove_all_containers
update_netprobe
update_macswitcher
reload_systemd
copy_old_database
replace_old_deploy
remove_olip_files
install_olip
deploy_olip
docker_prune

exit 0