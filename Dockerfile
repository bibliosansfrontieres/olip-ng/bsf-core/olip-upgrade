FROM debian:bookworm-slim

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates=20230311 \
    curl=7.88.1-10+deb12u8 && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN install -m 0755 -d /etc/apt/keyrings && \
    curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc && \
    chmod a+r /etc/apt/keyrings/docker.asc && \
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
    $(grep VERSION_CODENAME /etc/os-release | cut -d= -f2 | tr -d '"') stable" | \
    tee /etc/apt/sources.list.d/docker.list > /dev/null

# hadolint ignore=DL3008
RUN apt-get update && apt-get install -y --no-install-recommends \
    systemd=252.31-1~deb12u1 \
    git=1:2.39.5-0+deb12u1 \
    bash=5.2.15-2+b7 \
    unzip=6.0-28 \
    jq=1.6-2.1 \
    docker-ce-cli \
    docker-compose-plugin \
    rsync \
    iputils-ping=3:20221126-1+deb12u1 && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN YQ_VERSION="v4.44.3" && \
    ARCH=$(uname -m) && \
    if [ "$ARCH" = "x86_64" ]; then \
        curl -fsSL "https://github.com/mikefarah/yq/releases/download/$YQ_VERSION/yq_linux_amd64" -o /usr/bin/yq; \
    elif [ "$ARCH" = "aarch64" ]; then \
        curl -fsSL "https://github.com/mikefarah/yq/releases/download/$YQ_VERSION/yq_linux_arm64" -o /usr/bin/yq; \
    else \
        echo "Unsupported architecture: $ARCH" && exit 1; \
    fi && \
    chmod +x /usr/bin/yq

WORKDIR /olip-upgrade

COPY scripts/ .

CMD ["bash", "upgrade.sh"]